<section class="comonSection blogSection2" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <h2 class="sectionTitle2">
                    <small>Whats new!</small>
                    Checkout our blog for updates.
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid text-center">
        <div id="theGrid" class="main">
            <section class="grid">
                <div class="grid__item singleBlog2 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                    <div class="sbHeader">
                        <h1 class="title title--preview"><a href="#">The Things we Lost in the Fire</a></h1>
                    </div>
                    <div class="sbMeta">
                        <div class="loader"></div>
                        <div class="sbCats"><a href="blog.html">Web Design</a></div>
                        <div class="sbAuth">
                            <a href="blog.html"><img src="../asset/images/blog/3.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sbFooter">
                        <a href="singleblog.html" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                        <a href="singleblog.html" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="grid__item singleBlog2 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                    <div class="sbHeader">
                        <h1 class="title title--preview"><a href="#">What Happens in the Brain?</a></h1>
                    </div>
                    <div class="sbMeta">
                        <div class="loader"></div>
                        <div class="sbCats"><a href="allblog.php">Development</a>, <a href="allblog.php">Mobile</a></div>
                        <div class="sbAuth">
                            <a href="blog.html"><img src="../asset/images/blog/1.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sbFooter">
                        <a href="singleblog.html" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                        <a href="singleblog.html" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="grid__item singleBlog2 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                    <div class="sbHeader biggi">
                        <h1 class="title title--preview"><a href="#">What Goes Around Comes Around</a></h1>
                    </div>
                    <div class="sbMeta">
                        <div class="loader"></div>
                        <div class="sbCats"><a href="allblog.php">UI UX</a></div>
                        <div class="sbAuth">
                            <a href="allblog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sbFooter">
                        <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                        <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="grid__item singleBlog2 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="450ms">
                    <div class="sbHeader">
                        <h1 class="title title--preview"><a href="#">The Hunger of a Teenager</a></h1>
                    </div>
                    <div class="sbMeta">
                        <div class="loader"></div>
                        <div class="sbCats"><a href="allblog.php">Responsive</a></div>
                        <div class="sbAuth">
                            <a href="allblog.php"><img src="../asset/images/blog/1.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sbFooter">
                        <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                        <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="grid__item singleBlog2 text-center hidden wow fadeInUp" data-wow-duration="700ms" data-wow-delay="500ms">
                    <div class="sbHeader">
                        <h1 class="title title--preview"><a href="#">The Things we Lost in the Fire</a></h1>
                    </div>
                    <div class="sbMeta">
                        <div class="loader"></div>
                        <div class="sbCats"><a href="allblog.php">Web Design</a></div>
                        <div class="sbAuth">
                            <a href="blog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sbFooter">
                        <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                        <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </section>
            <section class="content">
                <div class="scroll-wrap">
                    <article class="content__item">
                        <section class="singleBlogPageHeader overlays signleBlogPageHeaderPopup">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="singleHeader text-center">
                                            <h2>The Things we Lost in the Fire</h2>
                                            <div class="cats"><a href="#">Web Design</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="singleInfo">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="authInfo">
                                                <img src="../asset/images/blog/2.png" alt=""/>
                                                by
                                                <a href="#">John Doe</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="singleInfos">
                                                <a href="#"><i class="esen-Calendar"></i>07 May</a>
                                                <a href="#"><i class="esen-Clock"></i>03 min read</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="singleBlogDetailsSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="blogArticels">
                                            <p class="excerpts">
                                                Conscience is the inner perception of objections to definite wish impulses 
                                                that exist in us; but the emphasis is put upon the fact that this rejection
                                                does not have to depend on anything else, that it is sure of itself.
                                            </p>
                                            <div class="details">
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit. 
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod 
                                                    mnesarchum omittantur. Eu liber dictas noluisse nam, ius ut ferri tibique.
                                                    No tota integre duo, cu usu adhuc quidam.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure 
                                                    commune fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. 
                                                    His ea quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus 
                                                    eu est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea.
                                                    No sit tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam 
                                                    placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur.
                                                </p>
                                                <div class="clearfix"></div>
                                                <div id="singleBlogSlider" class="slide carousel singleBlogSlider" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#singleBlogSlider" role="button" data-slide="prev">
                                                        <img src="../asset/images/slLeft.png" alt=""/>
                                                    </a>
                                                    <a class="right carousel-control" href="#singleBlogSlider" role="button" data-slide="next">
                                                        <img src="../asset/images/slRight.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="copys text-center">An per veniam libris conclusionemque, usu an paulo rebum omittam laboramus et pro voluptatibus. </div>
                                                <p class="quotes">
                                                    The emphasis is put upon the fact that this rejection does not have to depend on anything else, that it is sure of itself.
                                                </p>
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit.
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod mnesarchum omittantur.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure commune 
                                                    fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. His ea 
                                                    quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus eu 
                                                    est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea. No sit
                                                    tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur. Assum admodum oportere 
                                                    his ut, eos ex iisque vidisse splendide, putant viderer nominavi duo ea. Has id 
                                                    dicit postea, cum eu mollis option senserit.
                                                </p>
                                                <p>
                                                    Et vix facilis epicuri, an vis nihil vivendo splendide. Ut tacimates sensibus 
                                                    reprehendunt pro. Mel te sale maiestatis, alii hinc pro ut. An per veniam libris
                                                    conclusionemque, usu an paulo voluptatibus. Admodum 
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="relatedPosts">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="relatedPostsHolder" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h1>Related Articles</h1>
                                            <div class="asowl">
                                                <div id="articlesSlider_1" class="articlesSlider owl-carousel">
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog  relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Happens in the Brain?</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Development</a>, <a href="blog.php">Mobile</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/1.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Goes Around Comes Around</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">UI UX</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">The Things we Lost in the Fire</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Web Design</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="owlControlla preva"><img src="../asset/images/rlLeft.png" alt=""/></div>
                                                <div class="owlControlla nexta"><img src="../asset/images/rlRight.png" alt=""/></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="commentSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <div class="col-lg-6 col-sm-6">
                                                <h3 class="commentHeading">11 Comments</h3>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="socialShare text-right">
                                                    <span>share</span>
                                                    <a href="#"><i class="icon-facebook"></i></a>
                                                    <a href="#"><i class="icon-twitter"></i></a>
                                                    <a href="#"><i class="icon-gplus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <ol class="commentLista" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co1.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co2.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="childrenns">
                                                    <li>
                                                        <div class="singleCommenta">
                                                            <img src="../asset/images/blog/co3.png" alt=""/>
                                                            <div class="comDetailsa">
                                                                <span>07 June, 2014</span>
                                                                <h3>John Williams <a href="#">Replay</a></h3>
                                                                <div class="comContenta">
                                                                    Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                                    Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                                    eget lacinia libero.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co4.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <div class="commentForms" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h3 class="commentsTitls">
                                                Leave a reply
                                            </h3>
                                            <form method="post" action="#" class="comForms">
                                                <input type="text" name="name" class="com_inputa" placeholder="NAME:"/>
                                                <input type="text" name="name" class="com_inputa marginal" placeholder="EMAIL:"/>
                                                <input type="text" name="name" class="com_inputa" placeholder="WEBSITE:"/>
                                                <textarea class="com_textarea" name="com_textarea" placeholder="MESSAGE:"></textarea>
                                                <input type="submit" class="com_submits" value="Submit Reply"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </article>
                    <article class="content__item">
                        <section class="singleBlogPageHeader overlays signleBlogPageHeaderPopup">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="singleHeader text-center">
                                            <h2>The Things we Lost in the Fire</h2>
                                            <div class="cats"><a href="#">Web Design</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="singleInfo">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="authInfo">
                                                <img src="../asset/images/blog/2.png" alt=""/>
                                                by
                                                <a href="#">John Doe</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="singleInfos">
                                                <a href="#"><i class="esen-Calendar"></i>07 May</a>
                                                <a href="#"><i class="esen-Clock"></i>03 min read</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="singleBlogDetailsSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="blogArticels">
                                            <p class="excerpts">
                                                Conscience is the inner perception of objections to definite wish impulses 
                                                that exist in us; but the emphasis is put upon the fact that this rejection
                                                does not have to depend on anything else, that it is sure of itself.
                                            </p>
                                            <div class="details">
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit. 
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod 
                                                    mnesarchum omittantur. Eu liber dictas noluisse nam, ius ut ferri tibique.
                                                    No tota integre duo, cu usu adhuc quidam.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure 
                                                    commune fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. 
                                                    His ea quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus 
                                                    eu est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea.
                                                    No sit tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam 
                                                    placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur.
                                                </p>
                                                <div class="clearfix"></div>
                                                <div id="singleBlogSlider_1" class="slide carousel singleBlogSlider" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="../asset/images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#singleBlogSlider_1" role="button" data-slide="prev">
                                                        <img src="../asset/images/slLeft.png" alt=""/>
                                                    </a>
                                                    <a class="right carousel-control" href="#singleBlogSlider_1" role="button" data-slide="next">
                                                        <img src="../asset/images/slRight.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="copys text-center">An per veniam libris conclusionemque, usu an paulo rebum omittam laboramus et pro voluptatibus. </div>
                                                <p class="quotes">
                                                    The emphasis is put upon the fact that this rejection does not have to depend on anything else, that it is sure of itself.
                                                </p>
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit.
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod mnesarchum omittantur.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure commune 
                                                    fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. His ea 
                                                    quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus eu 
                                                    est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea. No sit
                                                    tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur. Assum admodum oportere 
                                                    his ut, eos ex iisque vidisse splendide, putant viderer nominavi duo ea. Has id 
                                                    dicit postea, cum eu mollis option senserit.
                                                </p>
                                                <p>
                                                    Et vix facilis epicuri, an vis nihil vivendo splendide. Ut tacimates sensibus 
                                                    reprehendunt pro. Mel te sale maiestatis, alii hinc pro ut. An per veniam libris
                                                    conclusionemque, usu an paulo voluptatibus. Admodum 
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="relatedPosts">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="relatedPostsHolder" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h1>Related Articles</h1>
                                            <div class="asowl">
                                                <div id="articlesSlider_2" class="articlesSlider owl-carousel">
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog  relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Happens in the Brain?</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Development</a>, <a href="blog.php">Mobile</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/1.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Goes Around Comes Around</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">UI UX</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <div class="loader"></div>
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">The Things we Lost in the Fire</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Web Design</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="../asset/images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="owlControlla preva"><img src="../asset/images/rlLeft.png" alt=""/></div>
                                                <div class="owlControlla nexta"><img src="../asset/images/rlRight.png" alt=""/></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="commentSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <div class="col-lg-6 col-sm-6">
                                                <h3 class="commentHeading">11 Comments</h3>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="socialShare text-right">
                                                    <span>share</span>
                                                    <a href="#"><i class="icon-facebook"></i></a>
                                                    <a href="#"><i class="icon-twitter"></i></a>
                                                    <a href="#"><i class="icon-gplus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <ol class="commentLista" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co1.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co2.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="childrenns">
                                                    <li>
                                                        <div class="singleCommenta">
                                                            <img src="../asset/images/blog/co3.png" alt=""/>
                                                            <div class="comDetailsa">
                                                                <span>07 June, 2014</span>
                                                                <h3>John Williams <a href="#">Replay</a></h3>
                                                                <div class="comContenta">
                                                                    Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                                    Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                                    eget lacinia libero.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="../asset/images/blog/co4.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <div class="commentForms" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h3 class="commentsTitls">
                                                Leave a reply
                                            </h3>
                                            <form method="post" action="#" class="comForms">
                                                <input type="text" name="name" class="com_inputa" placeholder="NAME:"/>
                                                <input type="text" name="name" class="com_inputa marginal" placeholder="EMAIL:"/>
                                                <input type="text" name="name" class="com_inputa" placeholder="WEBSITE:"/>
                                                <textarea class="com_textarea" name="com_textarea" placeholder="MESSAGE:"></textarea>
                                                <input type="submit" class="com_submits" value="Submit Reply"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </article>
                    <article class="content__item">
                        <section class="singleBlogPageHeader overlays signleBlogPageHeaderPopup">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="singleHeader text-center">
                                            <h2>The Things we Lost in the Fire</h2>
                                            <div class="cats"><a href="#">Web Design</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="singleInfo">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="authInfo">
                                                <img src="../asset/images/blog/2.png" alt=""/>
                                                by
                                                <a href="#">John Doe</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="singleInfos">
                                                <a href="#"><i class="esen-Calendar"></i>07 May</a>
                                                <a href="#"><i class="esen-Clock"></i>03 min read</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="singleBlogDetailsSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="blogArticels">
                                            <p class="excerpts">
                                                Conscience is the inner perception of objections to definite wish impulses 
                                                that exist in us; but the emphasis is put upon the fact that this rejection
                                                does not have to depend on anything else, that it is sure of itself.
                                            </p>
                                            <div class="details">
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit. 
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod 
                                                    mnesarchum omittantur. Eu liber dictas noluisse nam, ius ut ferri tibique.
                                                    No tota integre duo, cu usu adhuc quidam.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure 
                                                    commune fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. 
                                                    His ea quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus 
                                                    eu est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea.
                                                    No sit tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam 
                                                    placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur.
                                                </p>
                                                <div class="clearfix"></div>
                                                <div id="singleBlogSlider_2" class="slide carousel singleBlogSlider_1" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#singleBlogSlider_2" role="button" data-slide="prev">
                                                        <img src="images/slLeft.png" alt=""/>
                                                    </a>
                                                    <a class="right carousel-control" href="#singleBlogSlider_2" role="button" data-slide="next">
                                                        <img src="images/slRight.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="copys text-center">An per veniam libris conclusionemque, usu an paulo rebum omittam laboramus et pro voluptatibus. </div>
                                                <p class="quotes">
                                                    The emphasis is put upon the fact that this rejection does not have to depend on anything else, that it is sure of itself.
                                                </p>
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit.
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod mnesarchum omittantur.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure commune 
                                                    fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. His ea 
                                                    quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus eu 
                                                    est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea. No sit
                                                    tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur. Assum admodum oportere 
                                                    his ut, eos ex iisque vidisse splendide, putant viderer nominavi duo ea. Has id 
                                                    dicit postea, cum eu mollis option senserit.
                                                </p>
                                                <p>
                                                    Et vix facilis epicuri, an vis nihil vivendo splendide. Ut tacimates sensibus 
                                                    reprehendunt pro. Mel te sale maiestatis, alii hinc pro ut. An per veniam libris
                                                    conclusionemque, usu an paulo voluptatibus. Admodum 
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="relatedPosts">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="relatedPostsHolder" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h1>Related Articles</h1>
                                            <div class="asowl">
                                                <div id="articlesSlider_3" class="articlesSlider owl-carousel">
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog  relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Happens in the Brain?</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Development</a>, <a href="blog.php">Mobile</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/1.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Goes Around Comes Around</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">UI UX</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <div class="loader"></div>
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">The Things we Lost in the Fire</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Web Design</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="owlControlla preva"><img src="images/rlLeft.png" alt=""/></div>
                                                <div class="owlControlla nexta"><img src="images/rlRight.png" alt=""/></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="commentSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <div class="col-lg-6 col-sm-6">
                                                <h3 class="commentHeading">11 Comments</h3>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="socialShare text-right">
                                                    <span>share</span>
                                                    <a href="#"><i class="icon-facebook"></i></a>
                                                    <a href="#"><i class="icon-twitter"></i></a>
                                                    <a href="#"><i class="icon-gplus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <ol class="commentLista" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co1.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co2.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="childrenns">
                                                    <li>
                                                        <div class="singleCommenta">
                                                            <img src="images/blog/co3.png" alt=""/>
                                                            <div class="comDetailsa">
                                                                <span>07 June, 2014</span>
                                                                <h3>John Williams <a href="#">Replay</a></h3>
                                                                <div class="comContenta">
                                                                    Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                                    Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                                    eget lacinia libero.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co4.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <div class="commentForms" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h3 class="commentsTitls">
                                                Leave a reply
                                            </h3>
                                            <form method="post" action="#" class="comForms">
                                                <input type="text" name="name" class="com_inputa" placeholder="NAME:"/>
                                                <input type="text" name="name" class="com_inputa marginal" placeholder="EMAIL:"/>
                                                <input type="text" name="name" class="com_inputa" placeholder="WEBSITE:"/>
                                                <textarea class="com_textarea" name="com_textarea" placeholder="MESSAGE:"></textarea>
                                                <input type="submit" class="com_submits" value="Submit Reply"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </article>
                    <article class="content__item">
                        <section class="singleBlogPageHeader overlays signleBlogPageHeaderPopup">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="singleHeader text-center">
                                            <h2>The Things we Lost in the Fire</h2>
                                            <div class="cats"><a href="#">Web Design</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="singleInfo">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="authInfo">
                                                <img src="images/blog/2.png" alt=""/>
                                                by
                                                <a href="#">John Doe</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="singleInfos">
                                                <a href="#"><i class="esen-Calendar"></i>07 May</a>
                                                <a href="#"><i class="esen-Clock"></i>03 min read</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="singleBlogDetailsSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="blogArticels">
                                            <p class="excerpts">
                                                Conscience is the inner perception of objections to definite wish impulses 
                                                that exist in us; but the emphasis is put upon the fact that this rejection
                                                does not have to depend on anything else, that it is sure of itself.
                                            </p>
                                            <div class="details">
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit. 
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod 
                                                    mnesarchum omittantur. Eu liber dictas noluisse nam, ius ut ferri tibique.
                                                    No tota integre duo, cu usu adhuc quidam.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure 
                                                    commune fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. 
                                                    His ea quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus 
                                                    eu est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea.
                                                    No sit tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam 
                                                    placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur.
                                                </p>
                                                <div class="clearfix"></div>
                                                <div id="singleBlogSlider_3" class="slide carousel singleBlogSlider" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#singleBlogSlider_3" role="button" data-slide="prev">
                                                        <img src="images/slLeft.png" alt=""/>
                                                    </a>
                                                    <a class="right carousel-control" href="#singleBlogSlider_3" role="button" data-slide="next">
                                                        <img src="images/slRight.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="copys text-center">An per veniam libris conclusionemque, usu an paulo rebum omittam laboramus et pro voluptatibus. </div>
                                                <p class="quotes">
                                                    The emphasis is put upon the fact that this rejection does not have to depend on anything else, that it is sure of itself.
                                                </p>
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit.
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod mnesarchum omittantur.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure commune 
                                                    fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. His ea 
                                                    quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus eu 
                                                    est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea. No sit
                                                    tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur. Assum admodum oportere 
                                                    his ut, eos ex iisque vidisse splendide, putant viderer nominavi duo ea. Has id 
                                                    dicit postea, cum eu mollis option senserit.
                                                </p>
                                                <p>
                                                    Et vix facilis epicuri, an vis nihil vivendo splendide. Ut tacimates sensibus 
                                                    reprehendunt pro. Mel te sale maiestatis, alii hinc pro ut. An per veniam libris
                                                    conclusionemque, usu an paulo voluptatibus. Admodum 
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="relatedPosts">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="relatedPostsHolder" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h1>Related Articles</h1>
                                            <div class="asowl">
                                                <div id="articlesSlider_4" class="articlesSlider owl-carousel">
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog  relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Happens in the Brain?</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Development</a>, <a href="blog.php">Mobile</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/1.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Goes Around Comes Around</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">UI UX</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">The Things we Lost in the Fire</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Web Design</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="owlControlla preva"><img src="images/rlLeft.png" alt=""/></div>
                                                <div class="owlControlla nexta"><img src="images/rlRight.png" alt=""/></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="commentSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <div class="col-lg-6 col-sm-6">
                                                <h3 class="commentHeading">11 Comments</h3>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="socialShare text-right">
                                                    <span>share</span>
                                                    <a href="#"><i class="icon-facebook"></i></a>
                                                    <a href="#"><i class="icon-twitter"></i></a>
                                                    <a href="#"><i class="icon-gplus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <ol class="commentLista" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co1.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co2.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="childrenns">
                                                    <li>
                                                        <div class="singleCommenta">
                                                            <img src="images/blog/co3.png" alt=""/>
                                                            <div class="comDetailsa">
                                                                <span>07 June, 2014</span>
                                                                <h3>John Williams <a href="#">Replay</a></h3>
                                                                <div class="comContenta">
                                                                    Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                                    Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                                    eget lacinia libero.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co4.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <div class="commentForms" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h3 class="commentsTitls">
                                                Leave a reply
                                            </h3>
                                            <form method="post" action="#" class="comForms">
                                                <input type="text" name="name" class="com_inputa" placeholder="NAME:"/>
                                                <input type="text" name="name" class="com_inputa marginal" placeholder="EMAIL:"/>
                                                <input type="text" name="name" class="com_inputa" placeholder="WEBSITE:"/>
                                                <textarea class="com_textarea" name="com_textarea" placeholder="MESSAGE:"></textarea>
                                                <input type="submit" class="com_submits" value="Submit Reply"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </article>
                    <article class="content__item">
                        <section class="singleBlogPageHeader overlays signleBlogPageHeaderPopup">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="singleHeader text-center">
                                            <h2>The Things we Lost in the Fire</h2>
                                            <div class="cats"><a href="#">Web Design</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="singleInfo">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="authInfo">
                                                <img src="images/blog/2.png" alt=""/>
                                                by
                                                <a href="#">John Doe</a>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="singleInfos">
                                                <a href="#"><i class="esen-Calendar"></i>07 May</a>
                                                <a href="#"><i class="esen-Clock"></i>03 min read</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="singleBlogDetailsSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="blogArticels">
                                            <p class="excerpts">
                                                Conscience is the inner perception of objections to definite wish impulses 
                                                that exist in us; but the emphasis is put upon the fact that this rejection
                                                does not have to depend on anything else, that it is sure of itself.
                                            </p>
                                            <div class="details">
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit. 
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod 
                                                    mnesarchum omittantur. Eu liber dictas noluisse nam, ius ut ferri tibique.
                                                    No tota integre duo, cu usu adhuc quidam.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure 
                                                    commune fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. 
                                                    His ea quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus 
                                                    eu est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea.
                                                    No sit tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam 
                                                    placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur.
                                                </p>
                                                <div class="clearfix"></div>
                                                <div id="singleBlogSlider_4" class="slide carousel singleBlogSlider" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="item active">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="itemSingle">
                                                                <img src="images/blog/sb1.jpg" alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="left carousel-control" href="#singleBlogSlider_4" role="button" data-slide="prev">
                                                        <img src="images/slLeft.png" alt=""/>
                                                    </a>
                                                    <a class="right carousel-control" href="#singleBlogSlider_4" role="button" data-slide="next">
                                                        <img src="images/slRight.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="copys text-center">An per veniam libris conclusionemque, usu an paulo rebum omittam laboramus et pro voluptatibus. </div>
                                                <p class="quotes">
                                                    The emphasis is put upon the fact that this rejection does not have to depend on anything else, that it is sure of itself.
                                                </p>
                                                <p>
                                                    Everti doctus est et, ei vis habemus tibique. Scriptorem theophrastus in sit.
                                                    Stet gubergren ex per, denique consetetur cotidieque ad nec. Ex usu ullamcorper
                                                    definitionem, ipsum munere complectitur cum cu, no eam quod mnesarchum omittantur.
                                                </p>
                                                <p>
                                                    Elit facer constituto ex vis, te vide perpetua rationibus nec. Nec iriure commune 
                                                    fastidii ei, ius erat suscipit inimicus te, nec ei enim mucius. His ea 
                                                    quando noluisse, id sea explicari forensibus, id pro prima latine deserunt. 
                                                    Te exerci pertinacia per, has autem posidonium ad, ut fugit dicant imperdiet his. 
                                                    Et zril lucilius vel. Vocent veritus appareat te his, doctus accumsan forensibus eu 
                                                    est, mundi altera cu cum.
                                                </p>
                                                <p>
                                                    Rebum iriure pertinax ut mea, sit ne animal vivendo. Eruditi neglegentur eam ea. No sit
                                                    tantas adipisci. Duo id delicata dissentias reformidans, eos hinc tamquam placerat id.
                                                </p>
                                                <p>
                                                    Corpora explicari nam at, recteque intellegam temporibus in eum, te ius recusabo 
                                                    argumentum repudiandae. An ius eius perfecto neglegentur. Assum admodum oportere 
                                                    his ut, eos ex iisque vidisse splendide, putant viderer nominavi duo ea. Has id 
                                                    dicit postea, cum eu mollis option senserit.
                                                </p>
                                                <p>
                                                    Et vix facilis epicuri, an vis nihil vivendo splendide. Ut tacimates sensibus 
                                                    reprehendunt pro. Mel te sale maiestatis, alii hinc pro ut. An per veniam libris
                                                    conclusionemque, usu an paulo voluptatibus. Admodum 
                                                </p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="relatedPosts">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="relatedPostsHolder" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h1>Related Articles</h1>
                                            <div class="asowl">
                                                <div id="articlesSlider_5" class="articlesSlider owl-carousel">
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog  relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Happens in the Brain?</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Development</a>, <a href="blog.php">Mobile</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/1.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">What Goes Around Comes Around</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">UI UX</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 owlItem">
                                                        <div class="singleBlog relatedBlog text-center">
                                                            <div class="sbHeader">
                                                                <h1><a href="singleblog.php">The Things we Lost in the Fire</a></h1>
                                                            </div>
                                                            <div class="sbMeta">
                                                                <div class="loader"></div>
                                                                <div class="sbCats"><a href="blog.php">Web Design</a></div>
                                                                <div class="sbAuth">
                                                                    <a href="blog.php"><img src="images/blog/2.png" alt=""/></a>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="sbFooter">
                                                                <a href="singleblog.php" class="pull-left"><i class="esen-Calendar"></i>07 May</a>
                                                                <a href="singleblog.php" class="pull-right sbflast"><i class="esen-Clock"></i>03 min read</a>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="owlControlla preva"><img src="images/rlLeft.png" alt=""/></div>
                                                <div class="owlControlla nexta"><img src="images/rlRight.png" alt=""/></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="commentSection">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <div class="col-lg-6 col-sm-6">
                                                <h3 class="commentHeading">11 Comments</h3>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="socialShare text-right">
                                                    <span>share</span>
                                                    <a href="#"><i class="icon-facebook"></i></a>
                                                    <a href="#"><i class="icon-twitter"></i></a>
                                                    <a href="#"><i class="icon-gplus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <ol class="commentLista" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co1.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co2.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="childrenns">
                                                    <li>
                                                        <div class="singleCommenta">
                                                            <img src="images/blog/co3.png" alt=""/>
                                                            <div class="comDetailsa">
                                                                <span>07 June, 2014</span>
                                                                <h3>John Williams <a href="#">Replay</a></h3>
                                                                <div class="comContenta">
                                                                    Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                                    Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                                    eget lacinia libero.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="singleCommenta">
                                                    <img src="images/blog/co4.png" alt=""/>
                                                    <div class="comDetailsa">
                                                        <span>07 June, 2014</span>
                                                        <h3>John Williams <a href="#">Replay</a></h3>
                                                        <div class="comContenta">
                                                            Vivamus ultrices ullamcorper tortor, nec pharetra massa sollicitudin nec. 
                                                            Integer posuere lacus et blandit pulvinar. Ut id fermentum purus. Etiam 
                                                            eget lacinia libero.
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="clearfix"></div>
                                        <div class="commentForms" data-wow-duration="700ms" data-wow-delay="300ms">
                                            <h3 class="commentsTitls">
                                                Leave a reply
                                            </h3>
                                            <form method="post" action="#" class="comForms">
                                                <input type="text" name="name" class="com_inputa" placeholder="NAME:"/>
                                                <input type="text" name="name" class="com_inputa marginal" placeholder="EMAIL:"/>
                                                <input type="text" name="name" class="com_inputa" placeholder="WEBSITE:"/>
                                                <textarea class="com_textarea" name="com_textarea" placeholder="MESSAGE:"></textarea>
                                                <input type="submit" class="com_submits" value="Submit Reply"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </article>
                </div>
                <button class="close-button"><span></span></button>
            </section>
        </div>
        <div class="row morebuttonRow wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
            <div class="col-lg-12 text-center">
                <a href="blog.html" class="learnMoreButton2">View more posts</a>
            </div>
        </div>
    </div>
</section>
<section class="comonSection subscriptionSection">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h5 class="sectionTitle2">Stay upto date, join our newsletter</h5>
                <form id="subscriptionsforms" method="post" action="#" class="subscribeForm subscribeForm2">
                    <input id="sub_email" type="email" name="subs" placeholder="ENTER YOUR EMAIL."/>
                    <button id="subs_submit" type="submit">Lets Talk</button>
                    <div class="clearfix"></div>
                    <p>We do not share your information with anyone.</p>
                </form>
            </div>
        </div>
    </div>
</section>
