<section class="comonSection features" id="features">
    <div class="halfImg wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
        <img src="../asset/images/bg/features.jpg" alt=""/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12 pull-right">
                <h2 class="sectionTitle2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                    <small>What we do</small>
                    We do it best
                </h2>
                <p class="subTitle wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                    We design identity systems, digital platforms and brand
                    that engage today’s consumer.
                </p>
                <div class="row">
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                        <div class="iconBox iconBox2 text-left">
                            <i class="esen-Computer"></i>
                            <h5>Web Design</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Integer rutrum justo ac volutpat sodales. 
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                        <div class="iconBox iconBox2 text-left">
                            <i class="esen-CreateNew"></i>
                            <h5>Development</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Integer rutrum justo ac volutpat sodales. 
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                        <div class="iconBox iconBox2 text-left noMarginBottom">
                            <i class="esen-Phone"></i>
                            <h5>Mobile</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Integer rutrum justo ac volutpat sodales. 
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="450ms">
                        <div class="iconBox iconBox2 text-left noMarginBottom">
                            <i class="esen-Camera"></i>
                            <h5>Photography</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Integer rutrum justo ac volutpat sodales. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
            <section class="comonSection funfacts2 ffSection">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-3 text-left wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <h2 class="sectionTitle2">
                    <small>Our Approach</small>
                    Design. Develop. Deploy.
                </h2>
            </div>
            <div class="col-lg-3 col-sm-3 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                <div class="counters counters2 text-center">
                    <i class="esen-BrowserWindow"></i>
                    <h2 class="timer" data-counter="452">452</h2>
                    <h4>Website Launched</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                <div class="counters counters2 text-center">
                    <i class="esen-Clock"></i>
                    <h2 class="timer" data-counter="1250">1,250</h2>
                    <h4>working Hours</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-3 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="450ms">
                <div class="counters counters2 text-center">
                    <i class="esen-Users"></i>
                    <h2 class="timer" data-counter="524">524</h2>
                    <h4>Satisfied Clients</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="comonSection whychoose overlay">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="sectionTitle2 white">
                    <small>Why choose Stellar?</small>
                    We listen. We discuss. We advise.
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="bootsTab">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="wagl">
                            <div class="itemContent text-center">
                                <i class="esen-Conversation"></i>
                                <h2>We are good listener</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum justo ac 
                                    volutpat sodaleset fames ac ante ipsum primis in. Duis ligula enim quis dui id,
                                    elementum sagittis ligula.
                                </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="wac">
                            <div class="itemContent text-center">
                                <i class="esen-CreateNew"></i>
                                <h2>We are creative</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum justo ac 
                                    volutpat sodaleset fames ac ante ipsum primis in. Duis ligula enim quis dui id,
                                    elementum sagittis ligula.
                                </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="wdot">
                            <div class="itemContent text-center">
                                <i class="esen-Clock"></i>
                                <h2>We deliver on time</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum justo ac 
                                    volutpat sodaleset fames ac ante ipsum primis in. Duis ligula enim quis dui id,
                                    elementum sagittis ligula.
                                </p>
                            </div>
                        </div>
                    </div>
                    <ul class="stellarTab" role="tablist">
                        <li role="presentation" class="active"><a href="#wagl" aria-controls="wagl" role="tab" data-toggle="tab">We are good listener</a></li>
                        <li role="presentation"><a href="#wac" aria-controls="wac" role="tab" data-toggle="tab">We are creative</a></li>
                        <li role="presentation"><a href="#wdot" aria-controls="wdot" role="tab" data-toggle="tab">We deliver on time</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>