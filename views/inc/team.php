            <section class="comonSection" id="team">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <h2 class="sectionTitle2">
                                <small>Meet the team</small>
                                People busy making your dream a reality.
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 text-center team2row">
                            <div class="singleTeam2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                                <img src="../asset/images/team/1.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div><div class="singleTeam2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                                <img src="../asset/images/team/5.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div><div class="singleTeam2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                                <img src="../asset/images/team/2.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div><div class="singleTeam2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="450ms">
                                <img src="../asset/images/team/3.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div><div class="singleTeam2 hidden wow fadeInUp" data-wow-duration="700ms" data-wow-delay="500ms">
                                <img src="../asset/images/team/6.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div><div class="singleTeam2 hidden wow fadeInUp" data-wow-duration="700ms" data-wow-delay="600ms">
                                <img src="../asset/images/team/4.jpg" alt=""/>
                                <div class="teamHoverContent"></div>
                                <div class="teamDetails">
                                    <a href="#">Billy C.</a>
                                    <span>Art Director</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                        <section class="comonSection infoSection">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-lg-offset-1">
                            <h1 class="biggerHeading">We are <span>Dreamers.</span></h1>
                        </div>
                        <div class="col-lg-5 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                            <p class="infoP">
                                Lorem ipsum dolor sit amet, consectetur adip iscing elit. Integer
                                rutrum justo ac volut pat primis in. Duis ligula enim quis dui id 
                                elem entum sagittis ligula.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="comonSection gallery">
                <div class="galImg">
                    <img src="images/gallery/gal.jpg" alt=""/>
                </div>
            </section>
            <div class="clearfix"></div>
            <section class="comonSection testimonial2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="sectionTitle2">
                                <small>Don't take our for it!</small>
                                Hear What Our Customers Say.
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <div id="testimonialSlider2" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="testiItem testiItem2">
                                            <p>
                                                <span>“</span> Thanks so much for the awesome customer service. So many companies, large and small, 
                                                have a lot to learn from you. Great job! <span>”</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="testiItem testiItem2">
                                            <p>
                                                <span>“</span> Thanks so much for the awesome customer service. So many companies, large and small, 
                                                have a lot to learn from you. Great job! <span>”</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="testiItem testiItem2">
                                            <p>
                                                <span>“</span> Thanks so much for the awesome customer service. So many companies, large and small, 
                                                have a lot to learn from you. Great job! <span>”</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="testiItem testiItem2">
                                            <p>
                                                <span>“</span> Thanks so much for the awesome customer service. So many companies, large and small, 
                                                have a lot to learn from you. Great job! <span>”</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <ol class="carousel-indicators">
                                    <li data-target="#testimonialSlider2" data-slide-to="0" class="active">
                                        <img src="images/testimonial/1.png" alt=""/>
                                        <h5>Rebecca Matthews</h5>
                                        <h6>CEo-nasfactor.com</h6>
                                    </li>
                                    <li data-target="#testimonialSlider2" data-slide-to="1">
                                        <img src="images/testimonial/2.png" alt=""/>
                                        <h5>Rebecca Matthews</h5>
                                        <h6>CEo - nasfactor.com</h6>
                                    </li>
                                    <li data-target="#testimonialSlider2" data-slide-to="2">
                                        <img src="images/testimonial/3.png" alt=""/>
                                        <h5>Rebecca Matthews</h5>
                                        <h6>CEo - nasfactor.com</h6>
                                    </li>
                                    <li data-target="#testimonialSlider2" data-slide-to="3">
                                        <img src="images/testimonial/4.png" alt=""/>
                                        <h5>Rebecca Matthews</h5>
                                        <h6>CEo - nasfactor.com</h6>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="comonSection clientSection">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="clients">
                                <img src="images/client/c1.png" alt=""/>
                            </div>
                            <div class="clients">
                                <img src="images/client/c2.png" alt=""/>
                            </div>
                            <div class="clients">
                                <img src="images/client/c3.png" alt=""/>
                            </div>
                            <div class="clients">
                                <img src="images/client/c4.png" alt=""/>
                            </div>
                            <div class="clients">
                                <img src="images/client/c5.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>