            <section class="comonSection portfolios2" id="portfolio">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <h2 class="sectionTitle2">
                                <small>Recent Case studies</small>
                                What we have done so far.
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="folioNav folioNav2">
                                <ul id="filter">
                                    <li class="active" data-group="all">All</li>
                                    <li data-group="webdesign">Web Design</li>
                                    <li data-group="mobile">Mobile</li>
                                    <li data-group="photography">Photography</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="folioGrid2" id="folioGrid2">
                    <div class="folioItem2" data-groups='["all", "photography", "mobile"]'>
                        <img src="../asset/images/folio/p1.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Photography Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Mobile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="folioItem2" data-groups='["all", "webdesign", "mobile"]'>
                        <img src="../asset/images/folio/p2.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Cooking Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Development</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="folioItem2" data-groups='["all", "mobile", "photography"]'>
                        <img src="../asset/images/folio/p3.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Agency Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Mobile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="folioItem2" data-groups='["all", "webdesign"]'>
                        <img src="../asset/images/folio/p4.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Agency Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Mobile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="folioItem2" data-groups='["all", "webdesign"]'>
                        <img src="../asset/images/folio/p5.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Agency Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Mobile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="folioItem2" data-groups='["all", "webdesign"]'>
                        <img src="../asset/images/folio/p6.jpg" alt=""/>
                        <div class="fI2Hover">
                            <div class="fI2content">
                                <h2><a href="singlefolio.html">Agency Website</a></h2>
                                <div class="fI2Cats">
                                    <a href="#">Photography</a><span>/</span><a href="#">Mobile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center marginFolioMores">
                            <a class="learnMoreButton2" href="#">
                                View More
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="comonSection callToAction2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <div class="callToActionContent2 text-center">
                                <h5>Work with us</h5>
                                <h1>Impressed?</h1>
                                <h3 class="white">Tell us about your next big idea!</h3>
                                <a href="#" class="stellarButton">Lets Talk</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>