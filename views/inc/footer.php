            <footer class="footer footer2" id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 wow zoomIn" data-wow-duration="700ms" data-wow-delay="300ms">
                            <div class="copyRight">
                                &copy; Stellar. All Rights Reserved.<span>|</span>Theme designed by Nasirwd.<span>|</span>nasfactor.com
                            </div>
                        </div>
                        <div class="col-lg-3 wow zoomIn" data-wow-duration="700ms" data-wow-delay="300ms">
                            <div class="footerSocial">
                                <a class="fac" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="ins" href="#"><i class="fa fa-instagram"></i></a>
                                <a class="lin" href="#"><i class="fa fa-linkedin"></i></a>
                                <a class="goo" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="twi" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="dri" href="#"><i class="fa fa-dribbble"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="subscriptionSuccess">
                <div class="subsNotice">
                    <i class="fa fa-thumbs-o-up closers"></i>
                    <div class="clearfix"></div>
                    <p class="closers">Subscription Request Successfully placed!</p>
                </div>
            </div>
            <div class="contactSuccess">
                <div class="consNotice">
                    <i class="fa fa-thumbs-o-up closers"></i>
                    <div class="clearfix"></div>
                    <p class="closers">Your Message successfully sent!</p>
                </div>
            </div>
        </div>
        <a id="backToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
        <!-- Include All JS -->
        <script type="text/javascript" src="../asset/js/jquery.js"></script>
        <script type="text/javascript" src="../asset/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../asset/js/mixer.js"></script>
        <script type="text/javascript" src="../asset/js/wow.min.js"></script>
        <script type="text/javascript" src="../asset/js/jquery.appear.js"></script>
        <script type="text/javascript" src="../asset/js/prettyPhoto.js"></script>
        <script type="text/javascript" src="../asset/js/jquery.shuffle.min.js"></script>
        <script type="text/javascript" src="../asset/js/owl.carousel.js"></script>
        <script type="text/javascript" src="../asset/js/mixer.js"></script>
        <script type="text/javascript" src="../asset/js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="../asset/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="../asset/js/classie.js"></script>
        <script type="text/javascript" src="../asset/js/blogpopup.js"></script>
        <script type="text/javascript" src="../asset/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="../asset/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="../asset/js/theme.js"></script>
    </body>

<!-- Mirrored from nasfactor.com/themes/stellar/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Mar 2017 15:56:01 GMT -->
</html>