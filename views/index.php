<!-- Header section -->
 <?php include_once('inc/header.php') ?>

<!-- home section -->
 <?php include_once('inc/home.php') ?>

<!--  features section -->
 <?php include_once('inc/fetures.php') ?>

<!-- Portfolio section -->
 <?php include_once('inc/portfolios.php') ?>

<!-- Team section -->
 <?php include_once('inc/team.php') ?>

<!-- Blog section -->
 <?php include_once('inc/blog.php') ?>

<!-- Contact section -->
 <?php include_once('inc/contact.php') ?>

<!-- Footer section -->
 <?php include_once('inc/footer.php') ?>